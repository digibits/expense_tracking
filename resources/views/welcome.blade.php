<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" ">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">



        <title>Expense Management System</title>
     
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html{
                scroll-behavior: smooth;
            }
            .containercontact {max-width: 50%; display: block; margin:0px auto; border-radius: 10%;}
          
        </style>
    </head>
    <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark  static-top">
  <div class="container">
    <a class="navbar-brand" href="welcome.html">
          <img src=".././public/image/account-normal-removebg-preview.png"style="height: 50px;" alt="Logo">
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/">Home
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#about">About
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#contact">Contact US
                <span class="sr-only">(current)</span>
              </a>
        </li>
     
        <li class="nav-item">
         <a href="/login" ><button type="button" class="btn btn-primary">Login</button></a>
         
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class=" h-50 d-block w-100" src=".././public/image/new.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>Digibits  Expense System</h5>
        <p>Expenses Everything you need </p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="h-50 d-block w-100" src=".././public/image/2.png" alt="Second slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>Digibits  Expense System</h5>
        <p>Expenses Everything you need </p>
      </div>
    </div>
    <div class="carousel-item">
      <img class=" h-50 d-block w-100" src=".././public/image/new4.jpg" alt="Third slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>Digibits  Expense System</h5>
        <p>Expenses Everything you need </p>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="container col-lg-7  col-sm-10"  id="about">
    <h1 class="text-center">About</h1>
    <p class="text-justify"  >
    Cras mattis hendrerit nisl et facilisis. Praesent elementum urna eros, at posuere nibh lobortis ut. Curabitur a leo vel ligula dapibus finibus a id enim. Nullam sit amet erat odio. Morbi volutpat tempor libero, aliquam dictum sem bibendum eget. Morbi sit amet felis sit amet urna ultricies commodo non ut odio. Mauris id ipsum eros. Nulla consequat tristique imperdiet.
Vivamus quis neque accumsan, fermentum odio vel, tristique odio. Etiam in tincidunt nisi, id imperdiet lorem. Nunc id sapien dolor. Phasellus id arcu et nisl placerat finibus rhoncus vel diam. Cras nec bibendum ligula, nec dapibus neque. Phasellus nec est vitae odio posuere condimentum. Donec convallis facilisis elit, ac auctor eros commodo nec. Quisque congue lectus felis.
Duis dignissim, lorem venenatis sodales porta, diam orci pulvinar urna, in ullamcorper nisl nisi at ipsum. Ut augue augue, tempor et faucibus et, suscipit vitae turpis. Proin vestibulum quis felis at vehicula. Nunc blandit nulla et quam accumsan facilisis. Vestibulum consectetur, ipsum in volutpat consectetur, magna augue interdum nisl, vitae viverra tortor dolor auctor nulla. Aliquam mattis orci purus, eget hendrerit eros vulputate quis. Morbi tincidunt, lacus eget accumsan pretium, sapien lorem auctor velit, in ornare lorem leo rhoncus odio. Suspendisse scelerisque ullamcorper laoreet. Sed malesuada dapibus purus quis fermentum. Morbi tristique, felis ac rhoncus dignissim, dolor nisl vulputate turpis, in rhoncus est velit sit amet lectus.

    </p>
</div>

  <div class="container col-lg-7 col-sm-10">

<div class="p-3 mb-2 bg-secondary text-white" id="contact">
  <h1 class="text-center">Contact Us</h1>
  <form action="login.php" method="POST" accept-charset="utf-8">
    <div class="form-group">
        <input type="text" name="name" class="form-control" placeholder="Name">
    </div>
    <div class="form-group">
      <input type="text" name="email" class="form-control" placeholder="Email">
  </div>
  <div class="form-group">
    <input type="text" name="subject" class="form-control" placeholder="SUbject">
</div>
    <div class="form-group">
      <textarea id="form7" class="md-textarea form-control" rows="3"placeholder="Message"></textarea>

    </div>
   
    <button type="submit" class="btn btn-success text-right" name="submit">Submit</button>
</form>
</div>
</div>


<div class="card">
  <div class="card-header" >
    <h5 align="center"  > Digibits Development Cell</h5>
  </div>
  <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p align="center">Copyrights Reserved by @Digibits & Team</p>
   
  </div>
</div>
    </body>
</html>


