
 <!DOCTYPE html>
 <html>
 <head>
     <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">

    <link rel="stylesheet" href=".././public/style/login_css.css" type="text/css">
    <title>Log In</title>
 </head>


 
 <body>
    <header id="header" class="">

        
    <div class="col-11 m-auto wrapper">
        <div class="header">
            <h1>DigiBits Expenses <br>Tracking System</h1>
        </div>
        <div class="form_wrapper col-lg-3 bg-white p-3">
            <div class="form ">
                <h2 class="pb-3">Log In</h2>

                    


                    
                <form action="login.php" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <input type="text" name="uname" class="form-control" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="password" name="pass" class="form-control" placeholder="Password">
                    </div>
                   
                    <button type="submit" class="btn btn-success text-right" name="submit">Log In</button>
                </form>
                

                

                          

                       

            </div>
        </div>
    </div>
  </header>
  
<div class="card">
    <div class="card-header" >
      <h5 align="center"  > Digibits Development Cell</h5>
    </div>
    <div class="card-body">
      <blockquote class="blockquote mb-0">
        <p align="center">Copyrights Reserved by @Digibits & Team</p>
     
    </div>
  </div>
 </body>
 </html>

