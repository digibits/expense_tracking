<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" ">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Expense System</title>
    </head>
    <body>
    <header>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark  static-top">
  <div class="container">
    <a class="navbar-brand" href="http://localhost/digibits_team/expense_tracking/public">
          <img src=".././public/image/account-normal-removebg-preview.png"style="height: 50px; alt="">
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Add Expenses</button>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add your Expenses Here </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        
          <div class="input-group mb-3">
  <div class="input-group-prepend">
    <label class="input-group-text" for="inputGroupSelect01">Choose Expense Type</label>
  </div>
  <select class="custom-select" id="inputGroupSelect01">
    <option selected></option>
    <option value="1">Travel</option>
    <option value="2">Food</option>
    <option value="3">Salary</option>
    <option value="3">Rent</option>
    <option value="3">Miscellaneous</option>
  </select>
  
</div>

          <div class="form-group">
            <label for="Price" class="col-form-label">Amount:</label>
            <input type="text" class="form-control" id="price">
          </div>
          <div class="form-group">
  <label for="purpose">Description:</label>
  <textarea class="form-control" rows="5" id="purpose"></textarea>
</div>
<div class="custom-file">
  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
  <label class="custom-file-label" for="customFileLang">Upload Receipt of Expenses</label>
</div>

          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>
          
        </form>
      </div>
     
        </li>
     
        <div class="btn-group">
  <button type="button" class="btn btn-info">Username</button>
  <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <div class="dropdown-menu">
 
    <a class="dropdown-item" href="#">Logout</a>
   
  </div>
      </ul>
    </div>
  </div>
</nav>

<!-- Dashboard -->
<div class="container">
  <div class="container d-flex justify-content-around flex-wrap">
  <!-- Container 1 -->
    <div class="card m-3 text-center border border-danger" style="width: 18rem;">
      <div class="card-body text-danger">
        <h5 class="card-title">Total Expense</h5>
        <h6 class="card-subtitle mb-2 text-danger">PKR. 50,005</h6>
        
      </div>
    </div>

    <!-- container 2 -->
    <div class="card m-3 text-center border border-primary" style="width: 18rem;">
      <div class="card-body text-primary">
        <h5 class="card-title">This Month</h5>
        <h6 class="card-subtitle mb-2 text-primary">PKR. 10,000</h6>
      </div>
    </div>

    <!-- Contianer 3 -->
    <div class="card m-3 text-center border border-success" style="width: 18rem;">
      <div class="card-body text-success">
        <h5 class="card-title">Total Approved</h5>
        <h6 class="card-subtitle mb-2 text-success">PKR. 20,000</h6>
      </div>
    </div>
  </div>
</div>

<div class="container ">
  <form action="">
    <div class="d-flex justify-content-end flex-wrap m-3">
      <div class="form-group m-1">
        <input type="date" name="from" id="">
      </div>
      <div class="form-group m-1">
        <input type="date" name="to" id="">
      </div>
      <div class="form-group m-1">
        <input type="date" name="from" id="">
      </div>
      <div class="form-group m-1">
        <input type="date" name="from" id="">
      </div>
      <input type="submit" name="submit" value="Filter" class="btn btn-success">
    </div>
  </form>
</div>

<div class="container">
  <table class="table table-light table-bordered table-striped text-center">
    <thead class="thead-dark">
      <tr>
        <th><input type="checkbox" name="" id=""></th>
        <th>Expense Type</th>
        <th>Amount</th>
        <th>Created On</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><input type="checkbox" name="" id=""></td>
        <td>Salery</td>
        <td>PKR. 50,000</td>
        <td>17-July-2020 14:15</td>
        <td>Pending</td>
        <td><a href="">Delete</a> | <a href="">Edit</a></td>
      </tr>
      <tr>
        <td><input type="checkbox" name="" id=""></td>
        <td>Salery</td>
        <td>PKR. 50,000</td>
        <td>17-July-2020 14:15</td>
        <td>Pending</td>
        <td><a href="">Delete</a> | <a href="">Edit</a></td>
      </tr>
      <tr>
        <td><input type="checkbox" name="" id=""></td>
        <td>Salery</td>
        <td>PKR. 50,000</td>
        <td>17-July-2020 14:15</td>
        <td>Pending</td>
        <td><a href="">Delete</a> | <a href="">Edit</a></td>
      </tr>
      <tr>
        <td><input type="checkbox" name="" id=""></td>
        <td>Salery</td>
        <td>PKR. 50,000</td>
        <td>17-July-2020 14:15</td>
        <td>Pending</td>
        <td><a href="">Delete</a> | <a href="">Edit</a></td>
      </tr>
    </tbody>
  </table>
</div>




</header>
<div class="card-header" >
  <h5 align="center"  > Digibits Development Cell</h5>
</div>
<div class="card-body">
  <blockquote class="blockquote mb-0">
    <p align="center">Copyrights Reserved by @Digibits & Team</p>
 
</div>
</div>
  </body>
</html>
