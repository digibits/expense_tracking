<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class expenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::create('expense',function(Blueprint $table){
        
                $table->increments('id');
                $table->integer('emp_id')->unsigned();
                $table->foreign('emp_id')->references('id')->on('employee');
                $table->integer('t_id')->unsigned();
                $table->foreign('t_id')->references('id')->on('type');
                $table->text('purpose');
                $table->double('amount');
              
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense');

    }
}
