<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class employees extends Migration
{
/**
* Run the migrations.
*
* @return void
*/
public function up()
{
Schema::create('employee', function (Blueprint $table) { 
    Schema::enableForeignKeyConstraints();
    $table->increments('id'); 
    $table->string('email'); 
    $table->string('name'); 
    $table->text('contact'); 
    $table->timestamps(); });

}
/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{
Schema::drop("employee");
}
}