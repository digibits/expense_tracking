<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('test', "tets");
Route::get('/send', function ()
{
    $to_name = "Tayyab Sajjad";
    $to_email = "umair@digibits.xyz";
    $data = array('name' => 'Tayyab Sajjad', 'body' => 'Hello Tayyab this is Test Mail');
    Mail::send('mail', $data, function ($message) use ($to_name, $to_email)
    {
        $message->to($to_email)->subject('Website Testing Email');
    });
    echo "Mail sent Successfully";
});

Route::view('login', "login");
Route::view('user', "userDashboard");
Route::view('admin', "adminDashboard");


